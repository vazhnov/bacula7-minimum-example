
## Documentation

* [A brief tutorial](https://bacula.org/7.0.x-manuals/en/main/Brief_Tutorial.html), with part «Adding a Second Client»;
* [Customizing the configuration files](https://bacula.org/7.0.x-manuals/en/main/Customizing_Configuration_F.html) — good description of configuration files.

## Copyright

Distrubuted under [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/) license or newer.

